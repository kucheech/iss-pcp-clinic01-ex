const path = require('path')
const express = require('express')
const expressWS = require('express-ws')

const app = express()
const appWS = expressWS(app) // add WS support

app.ws('/chat', function (ws, req) {
  ws.on('message', function (message) {
    console.log('message: ' + message)
    ws.send('echo: ' + message.toUpperCase())
    process.nextTick(function(){
      
    })
  })

  ws.on('open', function () {
    console.log('open')
  })
  ws.on('close', function () {
    console.log('close')
  })
  ws.on('error', function () { })
})

app.use(express.static(path.join(__dirname, '../public')))
app.use(express.static(path.join(__dirname, '../bower_components')))

const PORT = process.env.PORT || 3000
app.listen(PORT, function () {
  console.log('Application started at port ' + PORT)
})
