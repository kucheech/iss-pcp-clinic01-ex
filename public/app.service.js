(function () {
  'use strict';

  angular
    .module('ChatApp')
    .service('ChatSvc', ChatSvc);

  ChatSvc.$inject = ["$rootScope"];
  function ChatSvc($rootScope) {
    var chatSvc = this;
    chatSvc.socket = null;
    chatSvc.connect = connect;
    chatSvc.send = send;


    ////////////////

    function send(message) {
      chatSvc.socket.send(message);
    }

    function connect(url) {
      // WebSocket is part of HTML5 API
      chatSvc.socket = new WebSocket(url + "/chat");
      chatSvc.socket.onopen = onopen;
      chatSvc.socket.onmessage = onmessage;
    }

    function onopen() {
      // function handler for onopen is executed outside of Angular
      // Angular would not have any knowledge of any changes to the model
      // start the digest cycle and perform the update inside the cycle
      console.log("ChatSvc socket onopen")
      $rootScope.$apply(function () {
        $rootScope.$broadcast('ChatApp.open'); //broadcast an event
      });
    }

    function onmessage(evt) {
      $rootScope.$apply(function () {
        $rootScope.$broadcast('ChatApp.message', evt.data); //broadcast an event
      });
    }

  }

})();