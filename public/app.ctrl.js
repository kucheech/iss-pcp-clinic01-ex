(function () {
  'use strict';

  angular
    .module('ChatApp')
    .controller('ChatCtrl', ChatCtrl);

  ChatCtrl.$inject = ["$scope", 'ChatSvc'];
  function ChatCtrl($scope, ChatSvc) {
    var vm = this;
    vm.chats = [];
    vm.message = '';
    vm.send = send;

    $scope.$on("ChatApp.open", function () {
      // console.log("ChatCtrl on ChatApp.open")
      vm.chats.unshift("Chat connection established");
    });

    $scope.$on("ChatApp.message", function (evt, message) {
      // console.log("ChatCtrl on ChatApp.message: ");
      // console.log(message);
      vm.chats.unshift(message);
    });

    init();

    ////////////////

    function init() {
      console.log("ChatCtrl init")
      ChatSvc.connect("ws://localhost:3000");
    }

    function send() {
      ChatSvc.send(vm.message);
      vm.message = "";
    }
  }
})();